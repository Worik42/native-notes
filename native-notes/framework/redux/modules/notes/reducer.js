import * as types from './types'

const initialState = {
    notes: [],
    selectNote: {},
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.NOTES_FULFILLED:
            return {
                ...state,
                notes: action.payload
            }
        case types.NOTE_COMPLEAT:
            return {
                ...state,
                ...action.payload
            }
        case types.NOTE_CREATE_FULFILLED:
            return {
                ...state,
                notes: state.notes.concat(action.payload),
            }
        case types.NOTE_DELETE_FULFILLED:
            return {
                ...state,
                notes: action.payload
            }
        case types.NOTE_EDIT:
            return {
                ...state,
                selectNote: action.payload
            }
        case types.NOTE_CREATE_REJECT:
            return {
                ...state
            }
        case types.NOTE_UPDATE_FULFILLED:
            return {
                ...state,
                notes: action.payload
            }
        case types.NOTE_UPDATE_REJECT:
            return {
                ...state
            }
        case types.CHANGE_TEXT_TITLE:
            return {
                ...state,
                selectNote: { ...state.selectNote, title: action.payload }
            }
        case types.CHANGE_TEXT_BODY:
            return {
                ...state,
                selectNote: { ...state.selectNote, body: action.payload }
            }
        default:
            return state
    }
}
