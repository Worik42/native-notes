import * as types from './types'
import { ActionCreators } from 'redux-undo'
import { CALL_API } from 'redux-api-middleware'


const process = (BASE_URL, url, method, body, token) => {
    let header = { 'Content-Type': 'application/json' }
    header = { ...header, 'access_token': token }
    return fetch(BASE_URL + url, {
        headers: header,
        method: method,
        body: body ? JSON.stringify(body) : {},
    })

}

const processWithoutBody = (BASE_URL, url, method,token) => {
    let header = { 'Content-Type': 'application/json' }
    header = { ...header, 'access_token': token }
    return fetch(BASE_URL + url, {
        headers: header,
        method: method,
    })

}



//const BASE_URL = 'http://10.0.2.2:3001/api'


export const getNotes = (token) => async (dispatch, getState, api) => {
    await dispatch({
        [CALL_API]: {
            endpoint: `${api.BASE_URL}/tasks`,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'access_token': token
            },
            types: [types.NOTES, types.NOTES_FULFILLED, types.NOTES_REJECT]
        }
    })
};




export const createNote = (token, title, body, done) => async (dispatch, getState, api) => {
    await dispatch({
        [CALL_API]: {
            endpoint: `${api.BASE_URL}/tasks`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'access_token': token
            },
            body: JSON.stringify({
                title: title,
                body: body,
                done: done
            }),
            types: [types.NOTE_CREATE, types.NOTE_CREATE_FULFILLED, types.NOTE_CREATE_REJECT]
        }
    })

}


export const deleteNote = (id, token) => async (dispatch, getState, api) => {
    let response = await processWithoutBody(api.BASE_URL,'/tasks/' + id, "DELETE",token);
    if (response.ok) {
        var state = getState().notes.present.notes;
        var position;
        const newState = state.map((value, key) => {
            if (state[key].id == id) {
                position = key;
            }
            return value
        });
        newState.splice(position, 1);
        dispatch({
            type: types.NOTE_DELETE_FULFILLED,
            payload: newState
        });
    } else {
        dispatch({
            type: types.NOTE_DELETE_REJECT,
            payload: state
        })
    }
}


export const noteUpdate = (id, done, title, body, token) => async (dispatch, getState,api) => {
    let response = await process(api.BASE_URL,'/tasks/' + id, 'PATCH', { id: id, done: done, body: body, title: title }, token)
    if (response.ok) {
        let rsp = await response.json()

        var state = getState().notes.present.notes;
        const newState = state.map((value, key) => {
            if (state[key].id == rsp.id) {
                value.title = rsp.title
                value.done = rsp.done
                value.body = rsp.body
                return value
            } else {
                return value
            }
        })

        dispatch({
            type: types.NOTE_UPDATE_FULFILLED,
            payload: newState
        })
    } else {
        dispatch({
            type: types.NOTE_UPDATE_REJECT,
            payload: state
        })
    }
}

export const onEdit = (id) => async (dispatch, getState) => {
    var state = getState().notes.present.notes;
    var note;
    state.forEach(element => {
        if (element.id === id)
            note = element
    });
    dispatch({
        type: types.NOTE_EDIT,
        payload: note,
    })

}

export const changeTextTitle = (text) => (dispatch, getState) => {
    var state = getState().notes.present.selectNote;
    dispatch({
        type: types.CHANGE_TEXT_TITLE,
        payload: text,

    })


}
export const changeTextBody = (text) => (dispatch, getState) => {
    var state = getState().notes.present.selectNote;
    dispatch({
        type: types.CHANGE_TEXT_BODY,
        payload: text,
    })


}





export const undo = () => (dispatch, getState) => {
    dispatch(ActionCreators.undo())
}

export const redo = () => (dispatch, getState) => {
    dispatch(ActionCreators.redo())
}