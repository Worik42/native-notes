import * as types from './types'
import{getErrorText} from '../../errorcode'
const process = (BASE_URL, url, method, body) => {
    let header = { 'Content-Type': 'application/json' }
    return fetch(BASE_URL + url, {
        headers: header,
        method: method,
        body: body ? JSON.stringify(body) : {},
    })

}


export const registration = (email, password) => async (dispatch, getState, api) => {
    let response = await process(api.BASE_URL, '/users', 'POST', { email, password })
    let rsp = await response.json()
    if (response.ok) {
        dispatch({
            type: types.REGISTRATION,
            payload: rsp
        })
    }
    else {
        let errorMessage = getErrorText(rsp.error.statusCode)
        dispatch({
            type: types.REGISTRATION_REJECT,
            payload: errorMessage
        })
    }
}
