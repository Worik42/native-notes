import * as types from './types'

const initialState = {
    errorMessage:''

}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.REGISTRATION:
            return {
                ...state,
                ...action.payload,
                errorMessage:''
            }
        case types.REGISTRATION_REJECT:
            return {
                ...state,
                errorMessage: action.payload
            }
        default:
            return state
    }
}
