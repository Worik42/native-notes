import * as types from './types'


const initialState = {
    errorMessage: ''
}
export default (state = initialState, action) => {
    switch (action.type) {
        case types.AUTH:
            return {
                ...state,
                ...action.payload,
                errorMessage: ''
            }
        case types.AUTH_REJECT:
            return {
                ...state,
                errorMessage: action.payload
            }
        default:
            return state
    }
}
