import * as types from './types'
import {getErrorText} from '../../errorcode'

//const BASE_URL = 'http://10.0.2.2:3001/api'


const process = (BASE_URL,url, method, body) => {
    let header = { 'Content-Type': 'application/json' }
    return fetch(BASE_URL + url, {
        headers: header,
        method: method,
        body: body ? JSON.stringify(body) : {},
    })

}


export const auth = (email, password) => async (dispatch, getState,api ) => {
    let response = await process(api.BASE_URL,'/users/login', 'POST', { email, password });
    let rsp = await response.json()
    if (response.ok) {
        api.storage.setItem('token', rsp.id)
        api.storage.setItem('email', email)
        dispatch({
            type: types.AUTH,
            payload: rsp
        })
       
        
    }else{
        let errorMessage = getErrorText(rsp.error.statusCode)
        dispatch({
            type: types.AUTH_REJECT,
            payload: errorMessage
        })
        
}
}

