import {
    createStore,
    applyMiddleware,
} from 'redux'
import { apiMiddleware } from 'redux-api-middleware'
import undoable, { includeAction, excludeAction } from 'redux-undo'
import thunk from 'redux-thunk'
import { combineReducers } from 'redux'
import auth from './modules/auth/reducer'
import registration from './modules/registration/reducer'
import notes from './modules/notes/reducer'
import * as types from './modules/notes/types'

import { composeWithDevTools } from 'remote-redux-devtools'
const store = (api) => createStore(
    combineReducers({
        auth,
        registration,
        notes: undoable(notes),
    }),
    composeWithDevTools(
        applyMiddleware(apiMiddleware, thunk.withExtraArgument(api)),
    ))

export default store
//let storage
//const setStorage = (_storage) => {
//    storage = _storage
//
//}
