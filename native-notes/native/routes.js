import React, { Component } from 'react'

import { StackNavigator } from 'react-navigation'

import Auth from './components/auth/index'
import Registration from './components/registration/index'
import Notes from './components/notes/index'
import { Provider } from 'react-redux'
import store from '../framework/redux/index'
import AuthLoadingScreen from './components/authLoadingScreen/index'
import AddNote from './components/addNote/index'
import EditNote from './components/editNote/index'

const Routes = StackNavigator(
    {
        AuthLoading: {
            screen: AuthLoadingScreen,
        },
        Auth: { screen: Auth },
        Registration: { screen: Registration },
        Notes: { screen: Notes,
        },
        AddNote: { screen: AddNote },
        EditNote: { screen: EditNote },

    },
    {
        initialRouteName: 'AuthLoading',

    });


export default Routes
