import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    AsyncStorage,
    Alert,
    
} from 'react-native'
import { connect } from 'react-redux'
import SnackBar from 'react-native-snackbar-component'
import { auth } from '../../../framework/redux/modules/auth/actions'
import { bindActionCreators } from 'redux'

function mapStateToProps(state, ownProps) {
    return state.auth
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ auth }, dispatch)
}

class Auth extends Component {
    static navigationOptions = {
        title: 'Авторизация',
      };
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {errorMessage: props.errorMessage, email: '', password: '' }
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit = () => {
        this.props.auth(this.state.email, this.state.password)
    };

    isEmpty = (obj) => {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }


    componentWillReceiveProps(props) {
        if (this.isEmpty(props.errorMessage)) { 
            this.props.navigation.replace('AuthLoading') }

        else
            this.setState({ errorMessage: props.errorMessage });

    }


    render() {
        let error
        if (!this.isEmpty(this.state.errorMessage))
        error =  <SnackBar
          visible={true}
          textMessage={this.state.errorMessage}
          distanceCallback={(distance)=>{this.setState({distance: distance});}}
        />
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInput} placeholder="login" onChangeText={(email) => this.setState({ email })} />
                <TextInput style={styles.textInput} placeholder="Password" secureTextEntry={true} onChangeText={(password) => this.setState({ password })} />
                <Button style={styles.buttonContainer} onPress={this.onSubmit} title="Авторизоваться" />
                <Button style={styles.buttonContainer} onPress={() =>  this.props.navigation.navigate('Registration')} title="Регистрация" />
                {error}
            </View>


        )


    }



}
export default connect(mapStateToProps, mapDispatchToProps)(Auth)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    textInput: {
        padding: 10,
        marginTop: 3,
    },


})
/*alignItems: 'center', 
*/