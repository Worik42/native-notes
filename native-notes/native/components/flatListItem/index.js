
import React, { Component } from 'react'
import { AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert, TouchableOpacity } from 'react-native'
import Swipeout from 'react-native-swipeout'
import CheckBox from 'react-native-check-box'
class FlatListItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeRowKey: null,
            id: props.item.id,
            done: props.item.done,
            title: props.item.title,
            body: props.item.body
        }
        this.actions = {
            onDelete: props.onDelete,
            onUpdate: props.onUpdate,
            onEdit: props.onEdit,
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            id: props.item.id,
            done: props.item.done,
            title: props.item.title,
            body: props.item.body
        });
    }


    render() {
        const swipeSettings = {
            autoClose: true,
            onClose: (secId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null })
                }
            },
            onOpen: (secId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item.key })
            },
            right: [
                {
                    onPress: () => {
                        const deletingRow = this.state.activeRowKey
                        Alert.alert(
                            'Alert',
                            'Are you sure you want to delete ?',
                            [
                                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                {
                                    text: 'Yes', onPress: () => {
                                        this.actions.onDelete(this.state.id)
                                    }
                                },
                            ],
                            { cancelable: true }
                        )
                    },
                    text: 'Delete', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        }
        return (
            <Swipeout style={styles.swipeout}  {...swipeSettings} >
                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={() => this.actions.onEdit(this.state.id)}>
                        <View style={styles.item}>
                            <View style={styles.initem}>
                                <Text style={styles.flatListItem}>{this.props.item.title}</Text>
                                <Text style={styles.flatListItem}>{this.props.item.body}</Text>
                                <CheckBox
                                    onClick={() =>
                                        this.actions.onUpdate(this.state.id, this.state.title, this.state.body, !this.state.done)}
                                    isChecked={this.state.done} />
                            </View>
                        </View>
                        <View style={styles.containersecond}>
                        </View>
                    </TouchableOpacity>
                </View>

            </Swipeout>

        )
    }
}

export default FlatListItem

const styles = StyleSheet.create({
    flatListItem: {
        color: 'white',
        padding: 10,
        fontSize: 16,
    },
    swipeout:{
        marginBottom:5,
        borderRadius:15,
    },
    checkBox: {},
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#78909C'
    },
    initem: {
        flex: 1,
        flexDirection: 'column',
        height: 150
    },
    containersecond: {
        height: 1,
        backgroundColor: 'white'
    },

})

