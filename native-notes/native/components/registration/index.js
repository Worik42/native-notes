import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
} from 'react-native'
import { registration } from '../../../framework/redux/modules/registration/actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SnackBar from 'react-native-snackbar-component'

function mapStateToProps(state, ownProps) {
    return state.registration
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ registration }, dispatch)
}

class Registration extends Component {
    static navigationOptions =  {
        title: 'Регистрация',
      };
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {errorMessage: props.errorMessage,  email: '', password: '' }
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit = () => {
        this.props.registration(this.state.email, this.state.password)
    };

    isEmpty = (obj) => {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }


    componentWillReceiveProps(props) {
        if (this.isEmpty(props.errorMessage)) { 
            this.props.navigation.goBack() }

        else
            this.setState({ errorMessage: props.errorMessage });

    }


    render() {
        let error
        if (!this.isEmpty(this.state.errorMessage))
        error =  <SnackBar
          visible={true}
          textMessage={this.state.errorMessage}
          distanceCallback={(distance)=>{this.setState({distance: distance});}}
        />
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInput} placeholder="email" onChangeText={(email) => this.setState({ email })} />
                <TextInput style={styles.textInput} placeholder="Password" secureTextEntry={true} onChangeText={(password) => this.setState({ password })} />
                <Button title="Регистрация" onPress={this.onSubmit} />
                {error}
            </View>
        )

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        width: 300,
    },

})
