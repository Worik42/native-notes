import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TextInput,
    Button,
    AsyncStorage,
} from 'react-native'
import { connect } from 'react-redux'
import { createNote, undo, redo } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ createNote, undo, redo }, dispatch)
}

class AddNote extends Component {
    static navigationOptions = {
        title: 'Добавить заметку',
      };
    constructor(props) {
        super(props)
        this.state = { title: '', body: '', done: false }
    }    


    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('token')

        this.props.createNote(userToken, this.state.title, this.state.body, this.state.done).then(() => {
            this.props.navigation.goBack()
        })
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerText}>
                    <TextInput placeholder="Name note" onChangeText={(title) => this.setState({ title })} />
                    <TextInput style={styles.textInput} multiline={true}
                        numberOfLines={4} onChangeText={(body) => this.setState({body})} />
                </View>    
                <Button style={styles.buttonContainer} onPress={this._bootstrapAsync} title="Create" />
            </View>


        )


    }



}
export default connect(mapStateToProps, mapDispatchToProps)(AddNote)

const styles = StyleSheet.create({
    container: {
     
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    containerText:{
        borderWidth: 1,
        margin:10,
    },
    textInput: {
        borderWidth: 3,
        borderColor: 'gray',
    },


})
/*alignItems: 'center', 
*/