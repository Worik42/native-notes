import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    Alert,
    AsyncStorage,
} from 'react-native'

import { HeaderBackButton } from 'react-navigation'
import { connect } from 'react-redux'
import { noteUpdate, undo, redo, changeTextTitle, changeTextBody } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'

function mapStateToProps(state, ownProps) {
    return state
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ noteUpdate, undo, redo, changeTextTitle, changeTextBody }, dispatch)
}

class EditNote extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (<HeaderBackButton onPress={navigation.getParam('isBack')} tintColor={'red'} />),
        title: 'Редактировать заметку',
    });
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.notes.present.selectNote.id,
            title: this.props.notes.present.selectNote.title,
            body: this.props.notes.present.selectNote.body,
            done: this.props.notes.present.selectNote.done,
            isChange: false,
        }

    }

    onBack = () => {
        if (!this.state.isChange)
            this.props.navigation.goBack()
        else {
            Alert.alert(
                'Предупреждение',
                'Выйти без сохранения',
                [
                    { text: 'Нет', style: 'cancel' },
                    {
                        text: 'Да', onPress: () => {
                            this.props.navigation.goBack()
                        }
                    },
                ]
            )
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ isBack: this.onBack });
    }


    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('token')

        this.props.noteUpdate(this.state.id, this.state.done, this.state.title, this.state.body, userToken).then(() => {
            this.props.navigation.goBack()
        })
    };

    undo = () => {
        this.props.undo()
    }
    redo = () => {
        this.props.redo()
    }

    onTitleChange = (text) => {
        this.props.changeTextTitle(text)
        this.state.isChange = true;
    }
    onBodyChange = (text) => {
        this.props.changeTextBody(text)
        this.state.isChange = true;
    }


    componentWillReceiveProps(props) {
        console.log(props)
        this.setState({
            title: props.notes.present.selectNote.title,
            body: props.notes.present.selectNote.body,
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerText}>
                    <TextInput placeholder="Name note" value={this.state.title} onChangeText={(text) => this.onTitleChange(text)} />
                    <TextInput style={styles.textInput} multiline={true} onChangeText={(text) => this.onBodyChange(text)} value={this.state.body}
                        numberOfLines={4} />
                </View>
                <Button style={styles.buttonContainer} onPress={this._bootstrapAsync} title="Update" />
                <Button style={styles.buttonContainer} onPress={this.undo} title="Undo" />
                <Button style={styles.buttonContainer} onPress={this.redo} title="Redo" />
            </View>


        )


    }



}
export default connect(mapStateToProps, mapDispatchToProps)(EditNote)

const styles = StyleSheet.create({
    container: {

        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    text: {
        color: 'black',

    },
    containerText: {
        borderWidth: 1,
        margin: 10,
    },
    textInput: {
        borderWidth: 3,
        borderColor: 'gray',
    },


})
/*alignItems: 'center', 
*/