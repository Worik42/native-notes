import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    AsyncStorage,
} from 'react-native'
import { connect } from 'react-redux'
import {  } from '../../../framework/redux/modules/auth/actions'
import { bindActionCreators } from 'redux'

function mapStateToProps(state, ownProps) {
    return state
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({  }, dispatch)
}

class DrawerContent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
        this.actions = {
            onLogout: props.onLogout,
          
        }

        this._bootstrapAsync()
    }


    _bootstrapAsync = async () => {
        var email = await AsyncStorage.getItem('email')
        this.state.email = email

    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.texts}>
                    <Text style={styles.containerText} >{this.state.email}</Text>
                </View>
                <Button style={styles.buttonContainer} onPress={() => this.actions.onLogout()} title="Logout" />
            </View>


        )


    }



}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    texts: {
        height:100,
        backgroundColor: 'orange',
        justifyContent: 'flex-end',
    },
    containerText: {
        margin: 10,
        fontSize: 25,
    },
    textInput: {
        borderWidth: 3,
        borderColor: 'gray',
    },


})
/*alignItems: 'center', 
*/