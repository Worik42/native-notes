import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    FlatList,
    Button,
    Easing
} from 'react-native'
import { getNotes, createNote, noteUpdate, deleteNote, onEdit, undo, redo } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import FlatListItem from '../flatListItem/index'
import Drawer from 'react-native-drawer-menu'
import DrawerContent from '../drawer/index'

import { YellowBox } from 'react-native'
function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        { getNotes, createNote, noteUpdate, deleteNote, onEdit, undo, redo },
        dispatch
    )
}


class Notes extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Заметки',
        headerRight: (
            <Button
                onPress={navigation.getParam('onAdd')}
                title="Add note"
                color="#000000"
            />
        ),
    });

    constructor(props) {
        super(props)
        this.state = {
            notes: props.notes.present.notes
        }
        this._bootstrapAsync()

    }

    onAddNote = () => {
        this.props.navigation.navigate('AddNote')
    }

    onDelete = async (id) => {
        const userToken = await AsyncStorage.getItem('token')
        this.props.deleteNote(id, userToken)
    };

    componentDidMount() {
        this.props.navigation.setParams({ onAdd: this.onAddNote });
    }

    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('token')

        this.props.getNotes(userToken)
    };

    componentWillReceiveProps(props) {
        this.setState({ notes: props.notes.present.notes });
    }


    onLogout = async () => {
        await AsyncStorage.clear()
        this.props.navigation.replace('Auth')
    };


    onUpdate = async (id, title, body, done) => {
        const userToken = await AsyncStorage.getItem('token')
        this.props.noteUpdate(id, done, title, body, userToken);
    }

    onEdit = (id) => {
        this.props.onEdit(id).then(this.props.navigation.navigate('EditNote'))

    }


    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '86%',
                    backgroundColor: '#CED0CE'
                }}
            />
        )
    };
    render() {
        let template
        if (this.state.notes.length > 1)
            template = <FlatList
                style={styles.flatListItems}
                data={this.state.notes}
                extraData={this.state}
                renderItem={({ item, index }) => {
                    return (
                        <FlatListItem item={item} index={index}
                            state={this.state.notes[index]}
                            onDelete={this.onDelete}
                            onUpdate={this.onUpdate}
                            onEdit={this.onEdit}
                            parentFlatList={this}
                        >

                        </FlatListItem>)
                }}
                keyExtractor={item => item.title}
                ItemSeparatorComponent={this.renderSeparator}
            />
        else
            template = <Text>To do is empty!</Text>

        return (<View  style={styles.container} >
            <Drawer
                style={styles.drawer}
                drawerContent={<DrawerContent
                    onLogout={this.onLogout}
                />}
                drawerWidth={250}
                type={Drawer.types.Default}
                easingFunc={Easing.ease}
                drawerPosition={Drawer.positions.Left}>
                <View style={styles.container}>
                    {template}
                </View>
            </Drawer>
        </View>
        )


    }



}
export default connect(mapStateToProps, mapDispatchToProps)(Notes)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    flatListItems: {
        padding: 5,
        paddingVertical: 20,

    },
    textInput: {
        borderWidth: 3,
        borderColor: 'gray',
        width: 300,
    },
    buttonAuth: {
        color: '#841584',
        width: 300,
    },
    drawer: {
        shadowColor: 'white',

    }

})
