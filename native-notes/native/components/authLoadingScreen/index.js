import React from 'react'
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native'

class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props)
        this._bootstrapAsync()

    }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
      const userToken = await AsyncStorage.getItem('token')
    
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      console.log(userToken)
      this.props.navigation.replace(userToken?'Notes':'Auth')
     // userToken ? this.props.navigation.replace('Notes') : this.props.navigation.replace('Auth')
  };

  // Render any loading content that you like here
  render() {
      return (
          <View style={styles.container}>
              <ActivityIndicator />
              <StatusBar barStyle="default" />
          </View>
      )
  }
}
export default AuthLoadingScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    textInput: {
        padding: 10,
        marginTop: 3,

    },


})