import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import store from '../framework/redux/index';
import { Provider } from 'react-redux'
import 'idempotent-babel-polyfill';

let api = {
  storage: localStorage,
  BASE_URL: 'http://localhost:3001/api'
}

ReactDOM.render((
  <Provider store={store(api)}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));