import React from "react";
import { Route, Switch } from "react-router-dom";

import Registration from './component/auth/registration/Registration';
import Notes from './component/notes/notes'
import Auth from './component/auth/Auth'
import EditNote from './component/editlNote/index'
class Main extends React.Component {
  
  render() {
    return (
      <main>
        <Switch>
          <Route exact path="/" component={Notes} />
          <Route exact path="/registration" component={Registration} />
          <Route exact path="/auth" component={Auth} />
          <Route exact path="/edit" component={EditNote}/> 
        </Switch>
      </main>
    );
  }
}

export default Main;
