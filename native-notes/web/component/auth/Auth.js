import React, { Component } from 'react';
import './style/Auth.css';
import { connect } from 'react-redux';

import { auth } from '../../../framework/redux/modules/auth/actions';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import SnackbarContent from '@material-ui/core/SnackbarContent';

class Auth extends Component {

    constructor(props) {
        super(props);
        this.state = { errorMessage: props.state.errorMessage, successMessage: '', email: '', password: '' };
    }

    handleEmailChange = (e) => {
        this.setState({ email: e.target.value });
    }
    handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }

    onSubmit = () => {
        this.props.auth(this.state.email, this.state.password);
    };
    componentWillReceiveProps(props) {
        if (this.isEmpty(props.state.errorMessage)) { 
            this.props.history.replace("/") }

        else
            this.setState({ errorMessage: props.state.errorMessage });

    }

    onRedirectToReg() {
        this.setState({ redirect: true });
    }
    onRegistration = () => {
        this.props.history.push("/registration");

    }
    isEmpty = (obj) => {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    render() {
        let error
        if (!this.isEmpty(this.state.errorMessage))
            error = <SnackbarContent message={this.state.errorMessage} />
        return (
            <div className='container'>
                <Paper >
                    <ul className='formAuth' >
                        <TextField type="text" name="title" className="text"
                            label="Email"
                            value={this.state.email}
                            onChange={this.handleEmailChange} id="title" />
                        <TextField id="password-input"
                            label="Пароль"
                            type="password"
                            className="text"
                            value={this.state.password}
                            onChange={this.handlePasswordChange} />
                    </ul>
                    <ul className='formBtn'>
                        <Button variant="contained" color="primary" className="authBtn" onClick={this.onSubmit} >Авторизоваться</Button>
                        <Button variant="contained" color="primary" className="authBtn" onClick={this.onRegistration} >Регистрация</Button>
                    </ul>
                </Paper>
                {error}
            </div>
        );

    }

}
export default connect(state => ({ state: state.auth }), { auth })(Auth);