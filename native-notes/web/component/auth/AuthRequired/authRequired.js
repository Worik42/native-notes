import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

export default function requiredAuth(Comp) {
  class AuthRequired extends Component {
    constructor(props) {
      super(props);
    }

   
    render() {
      const token = localStorage.getItem('token')
      console.log(token)
      if (token === ""||token === null||token === undefined) {
        return <Redirect to="/auth" />;
      } else {
       
        return <Comp history={this.props.history} />;
      }
    }
  }
  return connect(state => ({ state: state.auth }))(AuthRequired);
}
