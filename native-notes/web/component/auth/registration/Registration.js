import React, { Component } from 'react';
import { connect } from 'react-redux';
import { registration } from '../../../../framework/redux/modules/registration/actions';
import { bindActionCreators } from 'redux'
import {withRouter } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import SnackbarContent from '@material-ui/core/SnackbarContent';
import '../style/Auth.css';
function mapStateToProps(state, ownProps) {
    return state.registration
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({registration }, dispatch)
}

class Registration extends Component {
 static defaultProps = {errorMessage : ''}
    constructor(props) {
        super(props);
        this.state = { errorMessage: props.errorMessage, successMessage: '', email: '', password: '' };
    }


    handleEmailChange = (e) => {
        this.setState({ email: e.target.value });
    }
    handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }

    onSubmit = ()  => {
        this.props.registration(
            this.state.email,
            this.state.password
        )
    }

    componentWillReceiveProps(props) {
        if (this.isEmpty(props.errorMessage))
            this.props.history.replace("/")
        else
            this.setState({ errorMessage: props.errorMessage });

    }
    isEmpty = (obj) => {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }


    render() {
        let error
        if (!this.isEmpty(this.state.errorMessage))
            error = <SnackbarContent message={this.state.errorMessage} />
        return (
            <div>
                <Paper >
                    <ul className='formAuth' >
                        <TextField type="text" name="title" className="text"
                            label="Email"
                            value={this.state.email}
                            onChange={this.handleEmailChange} id="title" />
                        <TextField id="password-input"
                            label="Пароль"
                            type="password"
                            className="text"
                            value={this.state.password}
                            onChange={this.handlePasswordChange} />
                    </ul>
                    <ul className='formBtn'>
                        <Button variant="contained" color="primary" className="authBtn" onClick={this.onSubmit} >Зарегистрироваться</Button>
                    </ul>
                </Paper>
                {error}
            </div>
        );
    }



}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Registration));