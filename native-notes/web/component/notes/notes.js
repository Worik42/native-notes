import React, { Component } from 'react';
import Note from './note';
import { connect } from "react-redux";
import { getNotes, createNote, noteUpdate, onEdit, deleteNote, undo, redo } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'
import authRequired from "../auth/AuthRequired/authRequired";
import "./style/style.css";
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CircularProgress from '@material-ui/core/CircularProgress';



function mapStateToProps(state, ownProps) {
  return state
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getNotes, createNote, onEdit, deleteNote, noteUpdate, undo, redo }, dispatch)
}

class Notes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      notes: props.notes.present.notes,
      email: '',
      progress:false,
      noteForm: {
        title: "",
        body: ""
      }
    };
    this._bootstrapAsync()
  }
  _bootstrapAsync = async () => {
    if(this.state.progress)
      this.setState(progress:false)
    const userToken = localStorage.getItem('token')
    const email = localStorage.getItem('email')
    this.state.email = email
    this.props.getNotes(userToken)
  };

  onDelete = id => {
    const userToken = localStorage.getItem('token')

    this.props.deleteNote(id, userToken);
  }

  onUpdate = (id, done, title, body) => {
    this.props.noteUpdate(id, done, title, body, this.props.auth.id)
  }

  onCompleat = (id, done, title, body) => {
    const userToken = localStorage.getItem('token')
    this.props.noteUpdate(id, !done, title, body, userToken);
  };



  createNote = () => {
    const userToken = localStorage.getItem('token')
    if (this.state.title === '')
      alert("Вы не ввели Заголовок");
    else
      this.props.createNote(
        userToken,
        this.state.noteForm.title,
        this.state.noteForm.body,
        false,
      );
    this.setState({
      noteForm: {
        title: "",
        body: ""
      }
    });
  }

  handleNotecreatorChange = event => {
    this.setState({
      noteForm: {
        ...this.state.noteForm,
        [event.target.id]: event.target.value,
      }
    });


  };


  handleClick = id => event => {
    this.state.notes.map((value, key) => {
      if (value.id === id) {
        this.setState({ title: value.title, body: value.body, id: value.id, done: value.done });
      }
      return value
    })
    this.handleOpenModal();
  }


  componentWillReceiveProps(props) {
    this.setState({ notes: props.notes.present.notes,
    progress:true });
  }
  logOut = () => {
    localStorage.clear()
    this.props.history.replace('/auth')

  }

  render() {
    let content
    if(!this.state.progress)
      content= <CircularProgress />
      else 
      content = this.state.notes.map((value, key) => (
        <Note index={key}
          state={this.state.notes[key]}
          onCompleat={this.onCompleat}
          onEdit={this.onEdit}
          onUpdate={this.onUpdate}
          onDelete={this.onDelete}
        />
      ))   
    return (
      <div className='root'>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Заметки
          </Typography>
            <ul className='headEmail'>
              <Typography variant="display1" color="inherit">
                {this.state.email}
              </Typography>
            </ul>
            <ul className='btnLogout'>
              <Button variant="contained" color="inherit"  onClick={this.logOut} >Выйти</Button>
            </ul>
          </Toolbar>
        </AppBar>
        <span className="containerAdd">
          <Card className='cardViews'>
            <CardContent>
              <TextField type="text" name="title" className="text"
                label="Название"
                value={this.state.noteForm.title}
                onChange={this.handleNotecreatorChange} id="title" />
              <TextField type="text" name="body" className="text" id="multiline-flexible"
                label="Содержимое"
                multiline
                rowsMax="4"
                value={this.state.noteForm.body}
                onChange={this.handleNotecreatorChange} id="body" />
            </CardContent>
            <CardActions>
              <Button variant="contained" color="primary" onClick={this.createNote}>Создать заметку</Button>
            </CardActions>

          </Card>
        </span>
        <ul className='ul_container'>
        {content}
        </ul>
      </div>
    );
  }





}
export default authRequired(connect(mapStateToProps, mapDispatchToProps)(Notes));