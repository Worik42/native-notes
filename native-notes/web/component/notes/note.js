import React, { Component } from "react";
import { connect } from "react-redux";
import { onEdit, noteUpdate, undo, redo } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'
import { Redirect, withRouter } from "react-router-dom";
import "./style/style.css";
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';

function mapStateToProps(state, ownProps) {
  return state
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ onEdit, noteUpdate, undo, redo }, dispatch)
}

class Note extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.state.id,
      done: props.state.done,
      title: props.state.title,
      body: props.state.body,
    }
    this.index = props.index;

    this.actions = {
      onDelete: props.onDelete,
      onUpdate: props.onUpdate,
      onCompleat: props.onCompleat,
    };

  }
  componentWillReceiveProps(props) {
    this.setState({
      id: props.state.id,
      done: props.state.done,
      title: props.state.title,
      body: props.state.body,
    });
  }


  editNote = () => {
    console.log(this.props)
    this.props.onEdit(this.state.id).then(() => this.props.history.push("/edit"))

  }

  render() {
    return (
      <Card className='noteCard' >
        <CardContent onClick={this.editNote}>
        <Typography gutterBottom noWrap variant="headline" component="h2" >{this.state.title}</Typography>
        <Typography gutterBottom noWrap  >{this.state.body}</Typography>
         </CardContent >
         <Checkbox
          checked={this.state.done}
          onChange={() => this.actions.onCompleat(this.state.id,
            this.state.done,
            this.state.title,
            this.state.body)}
        />
        <CardActions>
        <Button variant="contained" color="secondary" onClick={() => this.actions.onDelete(this.state.id)}>Удалить</Button>
        </CardActions>
      </Card>
    );
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Note))
