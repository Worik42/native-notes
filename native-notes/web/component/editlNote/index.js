import React, { Component, ReactText } from "react";
import { connect } from "react-redux";
import { onEdit, noteUpdate, undo, redo, changeTextTitle, changeTextBody } from '../../../framework/redux/modules/notes/actions'
import { bindActionCreators } from 'redux'
import { withRouter } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import "./style.css";
function mapStateToProps(state, ownProps) {
    return state
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ onEdit, noteUpdate, undo, redo, changeTextTitle, changeTextBody }, dispatch)
}

function Transition(props) {
    return <Slide direction="up" {...props} />;
  }
  

class EditNote extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.notes.present.selectNote.id,
            done: props.notes.present.selectNote.done,
            title: props.notes.present.selectNote.title,
            body: props.notes.present.selectNote.body,
            isChange: false,
            openDialog: false
        }

    }
    componentWillReceiveProps(props) {
        this.setState({
            id: props.notes.present.selectNote.id,
            done: props.notes.present.selectNote.done,
            title: props.notes.present.selectNote.title,
            body: props.notes.present.selectNote.body,
        });
    }


    handleTitleChange = (e) => {
        this.props.changeTextTitle(e.target.value)
        this.state.isChange = true;
    }
    handleBodyChange = (e) => {
        this.props.changeTextBody(e.target.value)
        this.state.isChange = true;
    }

    handleClickOpen = () => {
        this.setState({ openDialog: true });
    };

    handleClose = () => {
        this.setState({ openDialog: false });
    };


    onCloseWithoutSave = () => {
        if (this.state.isChange) {
            this.handleClickOpen()
        } else
            this.props.history.goBack()
    }

    onUpdateAndBack = () => {
        const userToken = localStorage.getItem('token')
        this.props.noteUpdate(this.state.id, this.state.done, this.state.title, this.state.body, userToken).then(() => {
            this.props.history.goBack()
        })
        this.handleClose()
    }

    undo = () => {
        this.props.undo();
    }
    redo = () => {
        this.props.redo()
    }


    render() {
        return (
            <div>
                <Card className='cardView' >
                    <CardContent className='contentContainer' >
                        <TextField type="text" name="title" className="text"
                            label="Заголовок"
                            value={this.state.title} onChange={this.handleTitleChange} id="title" />
                        <TextField type="text" name="body" className="text" id="multiline-flexible"
                            label="Содержимое"
                            multiline
                            rowsMax="4"
                            value={this.state.body} onChange={this.handleBodyChange} id="body" />
                    </CardContent>
                    <CardActions className='contentContainer'>
                        <Button variant="contained" onClick={this.onUpdateAndBack} color="primary" >Сохранить</Button>
                        <Button variant="contained" color="inherit" onClick={this.undo}>Назад</Button>
                        <Button variant="contained" color="inherit" onClick={this.redo}>Вперед</Button>
                        <Button variant="contained" onClick={this.onCloseWithoutSave} color="secondary" >Закрыть</Button>

                    </CardActions>
                </Card>
                <Dialog
          open={this.state.openDialog}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {"Вы хотите выйти без сохранения?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Если вы выйдете , то ваши изменения не сохранятся.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Отмена
            </Button>
            <Button onClick={this.onUpdateAndBack} color="primary">
              Сохранить
            </Button>
            <Button onClick={() => this.props.history.goBack()} color="primary">
              Выйти
            </Button>
          </DialogActions>
        </Dialog>
            </div>
        )
    }


}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditNote))