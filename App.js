/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    AsyncStorage
} from 'react-native'
import { Provider } from 'react-redux'
import Routes from './native-notes/native/routes'
import store from './native-notes/framework/redux/index'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
})
let api = {
    storage: AsyncStorage,
    BASE_URL: 'http://10.0.2.2:3001/api'
  }
  
type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store(api)}>
            <Routes/>
            </Provider>
        )
    }
}

